import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IntroComponent } from './intro/intro.component';
import { LieuxComponent } from './lieux/lieux.component';
import { MainComponent } from './main/main.component';
import { PjComponent } from './pj/pj.component';
import { PnjComponent } from './pnj/pnj.component';
import { ResumeComponent } from './resume/resume.component';

const routes: Routes = [
  {path: "",redirectTo:"/main", pathMatch: 'full' },
  {path: "main", component: MainComponent},
  {path: "intro", component: IntroComponent},
  {path: "resume", component: ResumeComponent},
  {path: "pj", component: PjComponent},
  {path: "pnj", component: PnjComponent},
  {path: "lieux", component: LieuxComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
