import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResumeComponent } from './resume/resume.component';
import { PjComponent } from './pj/pj.component';
import { initializeApp } from "firebase/app";
import { PnjComponent } from './pnj/pnj.component';
import { LieuxComponent } from './lieux/lieux.component';
import { IntroComponent } from './intro/intro.component';
import { MainComponent } from './main/main.component';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBuN_jDY39bLjWADXI28wHg4QXhTie9LmE",
  authDomain: "nextroll-66076.firebaseapp.com",
  projectId: "nextroll-66076",
  storageBucket: "nextroll-66076.appspot.com",
  messagingSenderId: "490028125460",
  appId: "1:490028125460:web:e9fa2c5d211fcbead20601"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    AppComponent,
    ResumeComponent,
    PjComponent,
    PnjComponent,
    LieuxComponent,
    IntroComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
